#include "COGBoxShape.h"

#include "COGTransform.h"

#include "Engine/Public/EngineInterface.h"

COGBoxShape::COGBoxShape(GameObject * pGO, float fWidth, float fHeight, Color pColor)
	: COGShape(pGO)
	, mWidth(fWidth)
	, mHeight(fHeight)
	, mColor(pColor)
{
}

ComponentType COGBoxShape::GetType() const
{
	return ComponentType::BoxShape;
}

void COGBoxShape::Render()
{
	COGTransform* pTransform = mGO->FindComponent<COGTransform>(ComponentType::Transform);

	Vector2 position = pTransform->GetPosition();

	Vector2 startPoint = { position.x - (mWidth / 2), position.y - (mHeight / 2) };
	Vector2 endPoint = { position.x + (mWidth / 2),  position.y + (mHeight / 2) };

	mEngine->DrawBox(startPoint, endPoint, mColor, 0);
}

float COGBoxShape::GetExtends(char pSide)
{
	COGTransform* pTransform = mGO->FindComponent<COGTransform>(ComponentType::Transform);

	Vector2 position = pTransform->GetPosition();

	if (pSide == 'l' || pSide == 'L')
	{
		return position.x - mWidth;
	}
	else if (pSide == 'r' || pSide == 'R')
	{
		return position.x + mWidth;
	}
	else if (pSide == 'd' || pSide == 'D')
	{
		return position.y + mHeight;
	}
	else if(pSide == 'u' || pSide == 'U')
	{
		return position.y - mHeight ;
	}
	else
	{
		return 0.0f;
	}
}
