#pragma once

#include "COGShape.h"

class COGBoxShape : public COGShape
{
public:

	COGBoxShape(GameObject* pGO, float fWidth, float fHeight, Color pColor);

	virtual ComponentType GetType() const;

	virtual void Render() override;

	float GetExtends(char pSide);

private:

	Color mColor;
	float mWidth;
	float mHeight;

};