#include "Math.h"

#include <iostream>

float Math::Lerp(float firstFloat, float secondFloat, float by)
{
	return firstFloat * (1 - by) + secondFloat * by;
}

Vector2 Math::Lerp(Vector2 firstVector, Vector2 secondVector, float by)
{
	float retX = Lerp(firstVector.x, secondVector.x, by);
	float retY = Lerp(firstVector.y, secondVector.y, by);
	return { retX, retY };
}

float Math::Magnitude(Vector2 v)
{
	return sqrt((v.x * v.x) + (v.y * v.y));
}

float Math::Dot(Vector2 v1, Vector2 v2)
{
	return (v1.x * v2.x) + (v1.y * v2.y);
}

Vector2 Math::Normalized(Vector2 v)
{
	return { v.x / Magnitude(v), v.y / Magnitude(v) };
}

Vector2 Math::Cross(Vector2 v1, Vector2 v2)
{
	return { (v1.x * v2.y) , -(v1.y * v2.x) };
}

Vector2 Math::VectorAdd(Vector2 v1, Vector2 v2)
{
	Vector2 result;
	result.x = v1.x + v2.x;
	result.y = v1.y + v2.y;
	return result;
}

Vector2 Math::VectorSubtract(Vector2 v1, Vector2 v2)
{
	Vector2 result;
	result.x = v1.x - v2.x;
	result.y = v1.y - v2.y;
	return result;
}

Vector2 Math::VectorMultiply(Vector2 v1, float scalar)
{
	Vector2 result;
	result.x = v1.x * scalar;
	result.y = v1.y * scalar;

	return result;
}

Vector2 Math::VectorReflection(Vector2 v, Vector2 n)
{
	Vector2 result = VectorMultiply(n, 2 * Dot(v, n));
	result = VectorSubtract(v, result);
	return result;
}
