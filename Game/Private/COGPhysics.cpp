#include "COGPhysics.h"

#include "GameObject.h"
#include "Math.h"

std::vector<COGPhysics*> COGPhysics::mPhysicsComponents;

COGPhysics::COGPhysics(GameObject * pGO, bool bGenerateCollisionEvents)
	: Component(pGO)
	, mGenerateCollisionEvents(bGenerateCollisionEvents)
{
}

ComponentType COGPhysics::GetType() const
{
	return ComponentType::Physics;
}

void COGPhysics::Initialize()
{
	mTransform = mGO->FindComponent<COGTransform>(ComponentType::Transform);
	mBoxShape = mGO->FindComponent<COGBoxShape>(ComponentType::BoxShape);
	mCircleShape = mGO->FindComponent<COGCircleShape>(ComponentType::CircleShape);

	AddToComponentVector(mPhysicsComponents);
}

void COGPhysics::Destroy()
{
	RemoveFromComponentVector(mPhysicsComponents);
}

void COGPhysics::Update(float fDeltaT)
{
	// integrate velocity
	Vector2& myPosition = mTransform->GetPosition();

	Vector2 velocityToAdd = Math::VectorMultiply(mVelocity, fDeltaT);

	myPosition = Math::VectorAdd(myPosition, velocityToAdd);

	// check collisions
	if (mGenerateCollisionEvents)
	{
		for (COGPhysics* pPhysicsOther : COGPhysics::mPhysicsComponents)
		{
			// do not collide with self
			if (pPhysicsOther == this)
			{
				continue;
			}

			// TODO - maybe have IsColliding produce a vector/struct that contains the normal and then pass that into OnCollision?
			const bool bResult = IsColliding(pPhysicsOther);

			if (bResult)
			{
				for (IPhysicsCollisionEvent* pCollisionEventListener : mCollisionEventListeners)
				{
					pCollisionEventListener->OnCollision(this, pPhysicsOther);
				}
			}
		}
	}
}

void COGPhysics::SetVelocity(Vector2 velocity)
{
	mVelocity = velocity;

	// Max Velocity
	if (mVelocity.y > 300.0f)
	{
		mVelocity.y = 300.0f;
	}

	// Min Velocity
	if (mVelocity.y < -300.0f)
	{
		mVelocity.y = -300.0f;
	}
}

Vector2 COGPhysics::GetVelocity()
{
	return mVelocity;
}

void COGPhysics::ClearVelocity()
{
	mVelocity = { 0.0f,0.0f };
}

Vector2 COGPhysics::GetNormal()
{
	return mNormal;
}

bool COGPhysics::IsColliding(COGPhysics * pOther)
{
	if (mCircleShape != nullptr && pOther->mBoxShape != nullptr)
	{
		// check a circle colliding with a box
		if (mCircleShape->GetExtends('L') < pOther->mBoxShape->GetExtends('R')
			&& mCircleShape->GetExtends('R') > pOther->mBoxShape->GetExtends('L')
			&& mCircleShape->GetExtends('U') < pOther->mBoxShape->GetExtends('D')
			&& mCircleShape->GetExtends('D') > pOther->mBoxShape->GetExtends('U'))
		{
			if (mTransform->GetPosition().x < kViewportWidth / 2)
			{
				mNormal = { 1, 0 };
			}
			else
			{
				mNormal = { -1, 0 };
			}

			return true;
		}

		// Boundaries Collision
		// Left
		if (mCircleShape->GetExtends('L') <= 0.0f)
		{
			mNormal = { 1, 0 };
			return true;
		}
		// Top
		else if (mCircleShape->GetExtends('U') <= 0.0f)
		{
			mNormal = { 0, 1 };
			return true;
		}
		// Right
		else if (mCircleShape->GetExtends('R') >= kViewportWidth)
		{
			mNormal = { -1, 0 };
			return true;
		}
		// Bottom
		else if (mCircleShape->GetExtends('D') >= kViewportHeight)
		{
			mNormal = { 0, -1 };
			return true;
		}
	}

	// we don't cover the other cases
	// maybe assert here?

	return false;
}

void COGPhysics::AddCollisionEventListener(IPhysicsCollisionEvent * pEvent)
{
	mCollisionEventListeners.push_back(pEvent);
}
