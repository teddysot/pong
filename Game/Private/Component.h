#pragma once

#include <vector>
#include <algorithm>

class GameObject;
class exEngineInterface;

// all the different types of components
enum class ComponentType : int
{
	Transform = 0,
	Physics,
	BoxShape,
	CircleShape,
	Bounce,
	Controller
};

// our base component class
class Component
{
public:

	Component(GameObject* pGO);

	virtual ComponentType GetType() const = 0;

	virtual void Initialize() = 0;
	virtual void Destroy() = 0;

protected:

	template<class T>
	inline void AddToComponentVector(std::vector<T*>& componentVector)
	{
		componentVector.push_back((T*)this);
	}

	template<class T>
	inline void RemoveFromComponentVector(std::vector<T*>& componentVector)
	{
		componentVector.erase(std::remove(componentVector.begin(), componentVector.end(), this), componentVector.end());
	}

protected:
	GameObject* mGO;

	exEngineInterface* mEngine;
};
