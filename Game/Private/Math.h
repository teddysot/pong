#pragma once
#include "Engine/Public/EngineTypes.h"

class Math
{
public:
	// https://stackoverflow.com/questions/33044848/c-sharp-lerping-from-position-to-position
	static float Lerp(float firstFloat, float secondFloat, float by);
	static Vector2 Lerp(Vector2 firstVector, Vector2 secondVector, float by);

	static float Magnitude(Vector2 v);
	static float Dot(Vector2 v1, Vector2 v2);

	static Vector2 Normalized(Vector2 v);
	static Vector2 Cross(Vector2 v1, Vector2 v2);

	static Vector2 VectorAdd(Vector2 v1, Vector2 v2);
	static Vector2 VectorSubtract(Vector2 v1, Vector2 v2);
	static Vector2 VectorMultiply(Vector2 v1, float scalar);
	static Vector2 VectorReflection(Vector2 v, Vector2 n);
};