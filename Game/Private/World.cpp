#include "World.h"

#include "GameObjectFactory.h"
#include "InputManager.h"

World::World(exEngineInterface* pEngine)
{
	mGameObjectFactory = new GameObjectFactory(pEngine);
}

void World::Create()
{
	InputManager::getInstance().Initialize();

	mGameObjects.push_back(mGameObjectFactory->CreatePaddle(
		{ 50.0f, 50.0f }
		, 20.0f
		, 100.0f
		, { 255,0,0,255 }
		, 1
	));
	mGameObjects.push_back(mGameObjectFactory->CreatePaddle(
		{ 750.0f, 50.0f }
		, 20.0f
		, 100.0f
		, { 0,255,0,255 }
		, 2
	));
	mGameObjects.push_back(mGameObjectFactory->CreateBall(
		{ 400.0f, 50.0f }
		, 12.0
		, { 0,0,255,255 }
	));
}

void World::Destroy()
{
	for (GameObject* pGameObject : mGameObjects)
	{
		delete pGameObject;
	}

	mGameObjects.clear();
}

void World::Update(float fDeltaT)
{
	// run update
	InputManager::getInstance().Update();

	for (COGController* pController : COGController::mControllerComponents)
	{
		pController->Update();
	}

	// run simulation first
	for (COGPhysics* pPhysics : COGPhysics::mPhysicsComponents)
	{
		pPhysics->Update(fDeltaT);
	}

	// then render everything
	for (COGShape* pShape : COGShape::mShapeComponents)
	{
		pShape->Render();
	}
}