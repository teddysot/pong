#pragma once

#include "Engine/Public/EngineTypes.h"
#include "GameObject.h"

class COGTransform : public Component
{
public:

	COGTransform(GameObject* pGO, Vector2 pPositon);

	virtual void Initialize() override;

	virtual void Destroy() override;

	virtual ComponentType GetType() const;

	Vector2& GetPosition();

private:

	Vector2 mPosition;

};