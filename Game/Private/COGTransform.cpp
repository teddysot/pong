#include "COGTransform.h"

COGTransform::COGTransform(GameObject * pGO, Vector2 pPositon)
	: Component(pGO)
	, mPosition(pPositon)
{
}

void COGTransform::Initialize()
{
}

void COGTransform::Destroy()
{
}

ComponentType COGTransform::GetType() const
{
	return ComponentType::Transform;
}

Vector2 & COGTransform::GetPosition()
{
	return mPosition;
}
