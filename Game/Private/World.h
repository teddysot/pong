#pragma once
#include <vector>

class GameObject;
class GameObjectFactory;
class exEngineInterface;

class World
{
public:
	World(exEngineInterface* pEngine);

	void Create();

	void Destroy();

	void Update(float fDeltaT);

private:

	std::vector<GameObject*> mGameObjects;
	GameObjectFactory* mGameObjectFactory;
};