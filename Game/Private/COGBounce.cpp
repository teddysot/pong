#include "COGBounce.h"
#include "COGPhysics.h"

#include "Math.h"

COGBounce::COGBounce(GameObject * pGO)
	: Component(pGO)
{
}

ComponentType COGBounce::GetType() const
{
	return ComponentType::Bounce;
}

void COGBounce::Initialize()
{
	COGPhysics* pPhysics = mGO->FindComponent<COGPhysics>(ComponentType::Physics);
	pPhysics->SetVelocity({ 100.0f,100.0f });

	// add myself to the list of objects that are interested in collisions
	// COGPhysics is able to interface with us because we inherit from IPhysicsCollisionEvent
	pPhysics->AddCollisionEventListener(this);
}

void COGBounce::Destroy()
{
}

void COGBounce::OnCollision(COGPhysics * pMe, COGPhysics * pOther)
{
	// tell pMe to bounce by setting our velocity to a new amount
	Vector2 velocity = Math::VectorReflection(pOther->GetVelocity(), pMe->GetNormal());

	pOther->SetVelocity(velocity);
}
