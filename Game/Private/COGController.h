#pragma once
#include "GameObject.h"

#include "COGPhysics.h"

class COGController : public Component
{
public:
	COGController(GameObject* pGO, int nPlayer);

	void Update();

	virtual ComponentType GetType() const;

	virtual void Initialize() override;

	virtual void Destroy() override;

public:

	static std::vector<COGController*> mControllerComponents;

private:
	COGPhysics* mPhysics;
	int mPlayer;
};