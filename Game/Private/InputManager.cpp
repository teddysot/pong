
#include "InputManager.h"

InputManager* Singleton<InputManager>::mInstance = nullptr;

void InputManager::Initialize()
{
	mBindings[GameAction::UP_LEFT] = SDL_SCANCODE_W;
	mBindings[GameAction::DOWN_LEFT] = SDL_SCANCODE_S;

	mBindings[GameAction::UP_RIGHT] = SDL_SCANCODE_UP;
	mBindings[GameAction::DOWN_RIGHT] = SDL_SCANCODE_DOWN;
}

void InputManager::Update()
{
	int nKeys;
	const Uint8 *pState = SDL_GetKeyboardState(&nKeys);

	for (int i = 0; i < GameAction::MAX; ++i)
	{
		int nBinding = mBindings[i];
		mDown[i] = pState[nBinding];
	}
}

bool InputManager::IsButtonPressed(GameAction button)
{
	return mDown[button];
}
