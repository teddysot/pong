#pragma once

#include "GameObject.h"
#include "IPhysicsCollisionEvent.h"

class COGBounce : public Component, public IPhysicsCollisionEvent
{
public:
	COGBounce(GameObject* pGO);

	virtual ComponentType GetType() const;

	virtual void Initialize() override;

	virtual void Destroy() override;

	virtual void OnCollision(COGPhysics* pMe, COGPhysics* pOther) override;

};