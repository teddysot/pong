#pragma once
#include "GameObject.h"

class COGShape : public Component
{
public:

	COGShape(GameObject* pGO);

	virtual void Initialize() override;

	virtual void Destroy() override;

	virtual void Render() = 0;

public:

	static std::vector<COGShape*> mShapeComponents;
};