#pragma once

#include "Component.h"

#include "Engine/Public/EngineInterface.h"

class GameObject
{
public:

	GameObject(exEngineInterface* pEngine);

	~GameObject();

	void Initialize();

	void AddComponent(Component* pComponent);

	template<class T>
	inline T * FindComponent(ComponentType eType)
	{
		for (Component* pComponent : mComponents)
		{
			if (pComponent->GetType() == eType)
			{
				return (T*)pComponent;
			}
		}

		return nullptr;
	}

	exEngineInterface* mEngine;

private:
	std::vector<Component*> mComponents;
};
