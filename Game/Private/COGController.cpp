#include "COGController.h"

#include "InputManager.h"

std::vector<COGController*> COGController::mControllerComponents;

COGController::COGController(GameObject * pGO, int nPlayer)
	: Component(pGO)
	, mPlayer(nPlayer)
{
}

void COGController::Update()
{
	if (mPlayer == 1)
	{
		if (InputManager::getInstance().IsButtonPressed(GameAction::UP_LEFT))
		{
			mPhysics->SetVelocity({ 0.0f, -300.0f });
		}
		else if (InputManager::getInstance().IsButtonPressed(GameAction::DOWN_LEFT))
		{
			mPhysics->SetVelocity({ 0.0f, 300.0f });
		}
		else
		{
			mPhysics->ClearVelocity();
		}
	}
	else if (mPlayer == 2)
	{
		if (InputManager::getInstance().IsButtonPressed(GameAction::UP_RIGHT))
		{
			mPhysics->SetVelocity({ 0.0f, -300.0f });
		}
		else if (InputManager::getInstance().IsButtonPressed(GameAction::DOWN_RIGHT))
		{
			mPhysics->SetVelocity({ 0.0f, 300.0f });
		}
		else
		{
			mPhysics->ClearVelocity();
		}
	}
}

ComponentType COGController::GetType() const
{
	return ComponentType::Controller;
}

void COGController::Initialize()
{
	mPhysics = mGO->FindComponent<COGPhysics>(ComponentType::Physics);

	AddToComponentVector(mControllerComponents);
}

void COGController::Destroy()
{
	RemoveFromComponentVector(mControllerComponents);
}
