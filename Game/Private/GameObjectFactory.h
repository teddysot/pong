#pragma once

#include "GameObject.h"

#include "COGBounce.h"
#include "COGBoxShape.h"
#include "COGCircleShape.h"
#include "COGController.h"
#include "COGPhysics.h"
#include "COGTransform.h"

#include "Engine/Public/EngineInterface.h"

class GameObjectFactory
{
public:
	GameObjectFactory(exEngineInterface* pEngine);

	GameObject* CreatePaddle(Vector2 pPositon, float fWidth, float fHeight, Color pColor, int nPlayer);

	GameObject* CreateBall(Vector2 pPositon, float fBallRadius, Color pColor);

protected:
	exEngineInterface* mEngine;
};