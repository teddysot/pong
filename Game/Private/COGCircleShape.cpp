#include "COGCircleShape.h"

#include "COGTransform.h"

COGCircleShape::COGCircleShape(GameObject * pGO, float fRadius, Color pColor)
	: COGShape(pGO)
	, mRadius(fRadius)
	, mColor(pColor)
{
}

ComponentType COGCircleShape::GetType() const
{
	return ComponentType::CircleShape;
}

void COGCircleShape::Render()
{
	COGTransform* pTransform = mGO->FindComponent<COGTransform>(ComponentType::Transform);

	Vector2 position = pTransform->GetPosition();

	mEngine->DrawCircle(position, mRadius, mColor, 0);
}

float COGCircleShape::GetRadius()
{
	return mRadius;
}

float COGCircleShape::GetExtends(char pSide)
{
	COGTransform* pTransform = mGO->FindComponent<COGTransform>(ComponentType::Transform);

	Vector2 position = pTransform->GetPosition();

	if (pSide == 'l' || pSide == 'L')
	{
		return position.x - mRadius;
	}
	else if (pSide == 'r' || pSide == 'R')
	{
		return position.x + mRadius;
	}
	else if (pSide == 'd' || pSide == 'D')
	{
		return position.y + mRadius;
	}
	else if (pSide == 'u' || pSide == 'U')
	{
		return position.y - mRadius;
	}

	return 0.0f;
}
