#pragma once
#include "IPhysicsCollisionEvent.h"
#include "Engine/Public/EngineTypes.h"
	
#include "COGTransform.h"
#include "COGBoxShape.h"
#include "COGCircleShape.h"

class COGPhysics : public Component
{
public:

	COGPhysics(GameObject* pGO, bool bGenerateCollisionEvents);

	virtual ComponentType GetType() const;

	virtual void Initialize() override;

	virtual void Destroy() override;

	virtual void Update(float fDeltaT);

	// Velocity
	void SetVelocity(Vector2 velocity);
	Vector2 GetVelocity();
	void ClearVelocity();

	Vector2 GetNormal();

	bool IsColliding(COGPhysics* pOther);

	void AddCollisionEventListener(IPhysicsCollisionEvent* pEvent);

public:

	static std::vector<COGPhysics*> mPhysicsComponents;

private:

	COGTransform* mTransform;
	COGBoxShape* mBoxShape;
	COGCircleShape* mCircleShape;

	Vector2 mVelocity;
	Vector2 mNormal;

	bool mGenerateCollisionEvents;
	std::vector<IPhysicsCollisionEvent*> mCollisionEventListeners;

};