#pragma once

#include "Singleton.h"
#include "Engine/Public/SDL.h"

enum GameAction : unsigned int
{
	UP_LEFT = 0,
	DOWN_LEFT,
	UP_RIGHT,
	DOWN_RIGHT,

	MAX
};

class InputManager : public Singleton<InputManager>
{
public:
	void Initialize();

	void Update();

	bool IsButtonPressed(GameAction button);

private:
	int mBindings[GameAction::MAX];
	bool mDown[GameAction::MAX];
	
};