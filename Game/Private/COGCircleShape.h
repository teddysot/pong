#pragma once

#include "COGShape.h"

class COGCircleShape : public COGShape
{
public:

	COGCircleShape(GameObject* pGO, float fRadius, Color pColor);

	virtual ComponentType GetType() const;

	virtual void Render() override;

	float GetRadius();

	float GetExtends(char pSide);

private:

	Color mColor;
	float mRadius;

};