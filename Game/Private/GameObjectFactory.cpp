#include "GameObjectFactory.h"

GameObjectFactory::GameObjectFactory(exEngineInterface* pEngine)
	: mEngine(pEngine)
{
}

GameObject * GameObjectFactory::CreatePaddle(Vector2 pPositon, float fWidth, float fHeight, Color pColor, int nPlayer)
{
	GameObject* pPaddle = new GameObject(mEngine);

	COGTransform* pTransform = new COGTransform(pPaddle, pPositon);
	pPaddle->AddComponent(pTransform);

	COGBoxShape* pBoxShape = new COGBoxShape(pPaddle, fWidth, fHeight, pColor);
	pPaddle->AddComponent(pBoxShape);

	COGPhysics* pPhysics = new COGPhysics(pPaddle, false);
	pPaddle->AddComponent(pPhysics);

	COGController* pController = new COGController(pPaddle, nPlayer);
	pPaddle->AddComponent(pController);

	pPaddle->Initialize();

	return pPaddle;
}

GameObject * GameObjectFactory::CreateBall(Vector2 pPositon, float fBallRadius, Color pColor)
{
	GameObject* pBall = new GameObject(mEngine);

	COGTransform* pTransform = new COGTransform(pBall, pPositon);
	pBall->AddComponent(pTransform);

	COGCircleShape* pBoxShape = new COGCircleShape(pBall, fBallRadius, pColor);
	pBall->AddComponent(pBoxShape);

	COGPhysics* pPhysics = new COGPhysics(pBall, true);
	pBall->AddComponent(pPhysics);

	COGBounce* pBounce = new COGBounce(pBall);
	pBall->AddComponent(pBounce);

	pBall->Initialize();

	return pBall;
}
