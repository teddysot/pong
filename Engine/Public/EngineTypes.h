//
// * ENGINE-X
//
// + Types.h
// representations of: 2D vector with floats, 2D vector with integers and RBGA color as four bytes
//

#pragma once

//-----------------------------------------------------------------
//-----------------------------------------------------------------

struct Vector2
{
	float x, y;
};

//-----------------------------------------------------------------
//-----------------------------------------------------------------

struct Color
{
	unsigned char mColor[4];
};
